;
(function(window, document, undefined) {
    var YUHFU = {};
    YUHFU.version = '0.1';
    window.YUHFU = YUHFU;
    YUHFU.Util = {
        trim: function(str) {
            return str.trim ? str.trim() : str.replace(/^\s+|\s$/g, '');
        },
    };
})(window, document);