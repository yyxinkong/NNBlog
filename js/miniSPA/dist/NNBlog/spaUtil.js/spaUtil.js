"use strict";

var spaUtil = function () {
    function getPage(insertPoint) {
        var url = location.hash.replace("#", "");
        /*
        if (url == "") {
            url = 'init';
        }
        */
        if (!window.router[url]) {
            url = 'notfound';
        }
        ajaxRequest("get", window.router[url].partial, function (xhr) {
            history.pushState(null, null);
            insertPoint.innerHTML = xhr.responseText;
            if (window.router[url].js != "" || !window.router[url].js) {
                loadJS(document.getElementsByTagName("body")[0], window.router[url].js);
            }
            if (window.router[url].css != "" || !window.router[url].css) {
                loadCSS(document.getElementsByTagName("body")[0], window.router[url].css);
            }
        }, null);
    }
    var ajaxRequest = function ajaxRequest(method, url, callback, param) {
        var xhr = new XMLHttpRequest();
        xhr.open(method, url, true);
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {
                callback(xhr);
            }
        };
        if (method.toLowerCase() == "get") {
            xhr.send(null);
        }
        if (method.toLowerCase() == "post") {
            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhr.send(param);
        }
    };

    function loadCSS(node, cssUrl) {
        if (node.nodeName.toLowerCase() != "head") {
            var links = node.getElementsByTagName("link");
            if (Array.isArray(Array.from(links)) && links.length != 0) {
                Array.from(links).forEach(function (item) {
                    node.removeChild(item);
                });
            }
        }
        if (cssUrl != null && cssUrl != undefined) {
            if (Array.isArray(cssUrl)) {
                cssUrl.forEach(function (item) {
                    var link = document.createElement("link");
                    link.href = item;
                    link.type = "text/css";
                    link.rel = "stylesheet";
                    node.appendChild(link);
                });
            } else {
                var link = document.createElement("link");
                link.href = cssUrl;
                link.type = "text/css";
                link.rel = "stylesheet";
                node.appendChild(link);
            }
        } else {
            console.error("loadCSS error:url is null or undefined!");
        }
    }

    function loadJS(node, jsUrl) {
        //清除原有script标签，只清除body中的，不清除head中的。

        if (node.nodeName.toLowerCase() != "head") {
            var scripts = node.getElementsByTagName("script");
            if (Array.isArray(Array.from(scripts)) && scripts.length != 0) {
                Array.from(scripts).forEach(function (item) {
                    node.removeChild(item);
                });
            }
        }
        //加载新的script标签

        if (jsUrl != null && jsUrl != undefined && jsUrl != "") {
            if (Array.isArray(jsUrl)) {
                jsUrl.forEach(function (item) {
                    var script = document.createElement("script");
                    script.src = item;
                    script.type = "text/javascript";
                    script.charset = "UTF-8";
                    node.appendChild(script);
                });
            } else {
                var script = document.createElement("script");
                script.src = jsUrl;
                script.type = "text/javascript";
                script.charset = "UTF-8";
                node.appendChild(script);
            }
        }
    }

    function init() {
        var initJsFiles = ["../js/miniSPA/router.js", "../js/miniSPA/app.js"];
        var initLoadPoint = document.getElementsByTagName("head")[0];
        loadJS(initLoadPoint, initJsFiles);
    }
    window.addEventListener("load", init, false);
    return { getPage: getPage };
}();
//# sourceMappingURL=spaUtil.js.map