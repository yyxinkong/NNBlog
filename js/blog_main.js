function operationLi(event){
	let node=event.target;
	if(node.className=='level1_li'){
		let o=node.nextElementSibling;
		if(o.hidden==true){
			o.hidden=false;
		}else{
			o.hidden=true;
		}
	}	
}

function praiseOperation(){
	let arrays=document.querySelectorAll("[class~='document_praise']");
	arrays.forEach((item)=>{
		item.addEventListener("click",()=>{
			if(item.classList.contains("document_add_praise")){
				item.classList.remove("document_add_praise");
				item.classList.add("document_minus_praise");
				let node=item.firstElementChild;
				console.log(node);
			}else{
				item.classList.remove("document_minus_praise");
				item.classList.add("document_add_praise");			
			}			
		},false);
	});
}

function init(){
	praiseOperation();
	
	let ul=document.querySelectorAll(".level1_ul")[0];		//获取第一级ul，获取时返回的是数组，由于确定只有一个，所以直接取第一个。
	ul.addEventListener("click",operationLi,false);
}
window.addEventListener("load",init,false);		//固定格式，引导启动，当页面加载完成后，运行init函数
